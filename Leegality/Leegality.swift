//
//  Leegality.swift
//  Leegality
//
//  Created by Prakhar Agrawal on 22/01/18.
//  Copyright © 2018 Grey Swift Private Limited. All rights reserved.
//

import Foundation
import UIKit

public protocol LeegalityProtocol {
    func getResult(value : [String : String])
}

public class Leegality: UIViewController, UIWebViewDelegate {
    
    public var url: String!
    public var delegate: LeegalityProtocol?
    public var loadSpinner: UIActivityIndicatorView!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        var requestUrl: URL
        let character: Character = "?"
        if(url.characters.contains(character)){
            requestUrl = URL(string: url+"&device=ios")!
        } else {
            requestUrl = URL(string: url+"?device=ios")!
        }
        let requestObj = URLRequest(url: requestUrl)
        let webView:UIWebView = UIWebView(frame: UIScreen.main.bounds)
        loadSpinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        let transform:CGAffineTransform = CGAffineTransform(scaleX: 2.0, y: 2.0)
        loadSpinner.transform = transform;
        loadSpinner.center = view.center
        webView.delegate = self
        webView.loadRequest(requestObj)
        self.view.addSubview(webView)
        self.view.addSubview(loadSpinner)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    public func webViewDidStartLoad(_ : UIWebView) {
        loadSpinner.startAnimating()
    }
    
    public func webViewDidFinishLoad(_ : UIWebView) {
        loadSpinner.stopAnimating()
    }
    
    public func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let scheme = request.url?.scheme
        if scheme == "leegality" {
            let string = request.url?.absoluteString
            let strings = string!.components(separatedBy: "://")[1].components(separatedBy: ":")
            var dict = [String: String]()
            dict[strings[0]] = strings[1].removingPercentEncoding!
            delegate?.getResult(value: dict)
            self.dismiss(animated: true, completion: nil)
        }
        return true
    }
}


