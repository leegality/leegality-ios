Pod::Spec.new do |s|

  s.name         = "Leegality"
  s.version      = "1.0.0"
  s.summary      = "Leegality framework to integrate eSigning functionality in iOS apps."
  s.homepage     = "https://www.leegality.com"
  s.license      = "MIT"
  s.author             = "Prakhar Agrawal"
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://github.com/leegalitygspl/Leegality-iOS.git", :tag => "1.0.0" }
  s.source_files     = "Leegality", "Leegality/**/*.{h,m,swift}"

end
